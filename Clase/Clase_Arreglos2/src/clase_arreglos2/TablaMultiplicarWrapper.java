/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase_arreglos2;

import java.util.Scanner;
import static java.util.regex.Pattern.matches;

/**
 *
 * @author Brayan Guaman
 */
public class TablaMultiplicarWrapper {
    static void mostrar(Integer[][] matrix){
        for(Integer[] multiplicando : matrix){
            for(Integer valor: multiplicando){
                System.out.print(valor+"\t");
            }
            System.out.print("\n");
        }
    }
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String ingreso;
        int numTabla;
        boolean primerIngreso = false;
        String a;
        do{
            System.out.print(primerIngreso ? "Debe Ingresar un valor numérico menor a 99 \n" : "");
            System.out.print("HASTA QUE TABLA DESEA VISUALIZAR: ");
            ingreso = teclado.next();
            primerIngreso = true;
                //se valida que la cadena contenga dígitos y máximo de dos cifras
        }while( !matches("[\\d]{1,2}",ingreso) ); 
        numTabla = Integer.valueOf(ingreso);
        Integer[][] tablaMultiplicar=new Integer[numTabla+1][numTabla+1];
        
        //se crea la tabla de multiplicar
        for(Integer multiplicando = 1; multiplicando <= numTabla; multiplicando++ ){
            //se va agregando en la fila y columna 0 los números a multiplicarse
            tablaMultiplicar[0][multiplicando]=multiplicando;
            tablaMultiplicar[multiplicando][0]=multiplicando;
            //se agregan los resultados de la multiplicación
            for(int multiplicador = 1; multiplicador <= numTabla ; multiplicador++ ){
                tablaMultiplicar[multiplicando][multiplicador]=multiplicando*multiplicador;
            }
        }
        TablaMultiplicarWrapper.mostrar(tablaMultiplicar);
        //se crea la tabla de multiplicar
//        for(int multiplicando = 1; multiplicando < tablaMultiplicar.length; multiplicando++ ){
//            //se va agregando en la fila y columna 0 los números a multiplicarse
//            tablaMultiplicar[0][multiplicando]=multiplicando;
//            tablaMultiplicar[multiplicando][0]=multiplicando;
//            //se agregan los resultados de la multiplicación
//            for(int multiplicador = 1; multiplicador < tablaMultiplicar.length ; multiplicador++ ){
//                tablaMultiplicar[multiplicando][multiplicador]=multiplicando*multiplicador;
//            }
//        }
    }
}
