/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase_arreglos2;

/**
 *
 * @author Brayan Guaman
 */
public class Cadenas {
    public static void main(String[] args){
        String cadena = new String("Hola Mundo");
        String cadena2 = new String("HOLA MUNDO");
        
        cadena= cadena + ". cmpo están\n";
        cadena2 = cadena.concat(".espero que bien");
        
        
        
        System.out.println(cadena);
       
        System.out.println(cadena2 );
        
        cadena=cadena2;
        System.out.println(cadena2 );
        
        cadena="PRIMERO C";
        cadena2="PRIMERO C";
        
        System.out.println( cadena2==(cadena) );
        System.out.println( cadena2.equals(cadena) );
        System.out.println( cadena2.equalsIgnoreCase(cadena) );
        
         cadena2 = "   PRIMERO C     ";
         
        System.out.println(cadena2.length());
        System.out.println(cadena2.toLowerCase());
        System.out.println(cadena2.trim());
        cadena2= cadena2.trim();
        System.out.println(cadena2.replace("R",""));
        System.out.println(cadena2.indexOf('E'));
        System.out.println(cadena2.lastIndexOf('C'));
        System.out.println(cadena2.substring(3,7));
        
        
        
    }
}
