/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorial;

import java.util.Scanner;
import static java.util.regex.Pattern.matches;

/**
 *
 * @author Zero
 */
public class FactorialDoWhile {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
	//byte numeroInferior,numeroSuperior;
	String numeroInferior, numeroSuperior;
        double factorial;
        Scanner teclado = new Scanner(System.in);
        do{
            
            System.out.println("Ingrese un valor numérico comprendido entre 1 y 170.\n"
                    + "El límite inferior debe ser menor al limite superior.");
            System.out.print("Ingrese el limite inferior: ");
            numeroInferior = teclado.next();
            System.out.print("Ingrese el limite superior: ");
            numeroSuperior = teclado.next();
        //verificación si los valores ingresados son números máximo de 2 cifras
	} while( !(matches("^\\d+",numeroInferior) && matches("^\\d+",numeroSuperior)
                && Short.valueOf(numeroInferior)>0 && Short.valueOf(numeroSuperior)<=170
                && Short.valueOf(numeroInferior) <= Short.valueOf(numeroSuperior)
                ));

        Short i=Short.valueOf(numeroInferior);
        do{
            factorial = 1;
            Short j = 1;;
            do{
                factorial *=j;
                j++;
            }while(j<=i);
            System.out.println(i+"! = "+factorial);
            i++;
        } while(i <=Short.valueOf(numeroSuperior));
    }
}
